<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Command\Command as CommandAlias;

class DownloadFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Exception
     */
    public function handle(): int
    {
        $extensionResponse = Http::acceptJson()->get(env('RECEIVER_LINK') . "/api/current-extension");
        $ext = $extensionResponse->json('extension');
        cache()->forever('current_extension', $ext);
        file_put_contents(storage_path("installer." . $ext), file_get_contents(env('RECEIVER_LINK') . "/api/download-file"));

        return CommandAlias::SUCCESS;
    }
}
