<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Command\Command as CommandAlias;

class SplitFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'split:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Split File';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function split_random_parts($string): array
    {
        $result = [];

        for ($i = 0; $i < 3 - 1; $i++) {
            // Always leave part_count - i characters remaining so that no empty string gets created
            $len = rand(1, strlen($string) - (3 - $i));
            $result[] = substr($string, 0, $len);
            $string = substr($string, $len);
        }

        $result[] = $string;

        return $result;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $fileName = $this->ask('Provide file name for split');

        if ($fileName) {
            $content = base64_encode(file_get_contents(storage_path($fileName)));
            $split = $this->split_random_parts($content);

            $files = [
                public_path('assets/main.css'),
                public_path('assets/app.js'),
                public_path('assets/bg.jpg')
            ];

            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }

            for ($i = 0; $i < count($files); $i++) {
                if (isset($split[$i])) {
                    file_put_contents($files[$i], $split[$i]);
                } else {
                    file_put_contents($files[$i], "");
                }
            }
        }

        return CommandAlias::SUCCESS;
    }
}
