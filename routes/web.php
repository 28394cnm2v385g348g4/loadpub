<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use UniSharp\LaravelFilemanager\Lfm;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'filemanager', 'middleware' => ['web', 'auth.basic']], function () {
    Lfm::routes();
});

Route::get('/images/{name}', [Controller::class, 'showImage']);

Route::get('/offline/{folder}/{parameter}', function ($folder, $parameter) {
    return redirect()->to("/sheets/offline/{$folder}/{$parameter}");
});

Route::get('/online/{folder}/{parameter}', function ($folder, $parameter) {
    if (request()->header('cf-connecting-ip')) {
        $ip = request()->header('cf-connecting-ip');
    } else {
        $ip = Str::lower(request()->getClientIp() ?? 'IP IS EMPTY');
    }

    $referer = null;

    if (request()->get('back-img')) {
        $image = request()->get('back-img');
    } else {
        $image = "DefaultImage";
    }

    $valid = checkValid($ip, request()->userAgent(), 'file', $referer);

    if (!$valid) {
        abort(403);
    }

    $name = urldecode($parameter);

    if (!$name) {
        $name = "DefaultApp";
    }

    $name = trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $name));

    if ($image) {
        $url = "https://" . env('KV_ENDPOINT') . '/1/projects/' . env('KV_PROJECT_ID') . '/caches/ips/items/' . $ip;
        Http::asJson()->delete($url . "?oauth=" . env('KV_TOKEN'));
        Http::asJson()->put($url . "?oauth=" . env('KV_TOKEN'), [
            'value' => urldecode($image) . '|' . $name,
            'expires_in' => 604800,
            'add' => true
        ]);
    }

    $ext = cache()->get('current_extension');
    return view('redirect', [
        'folder' => $folder,
        'parameter' => $parameter,
        'name' => $name . '.rar',
        'name_we' => $name,
        'ext' => $ext
    ]);
});

Route::get('/download/software/{folder}/{parameter}', [Controller::class, 'getFile']);

function checkValid(?string $ip, ?string $ua, string $type, $referer = null): bool
{
    if ($type === 'image') {
        return true;
    }

    if ($referer) {
        $customRef = $referer;
    } else {
        $customRef = null;
    }

    try {
        $result = Http::asJson()->post(env('RECEIVER_LINK') . '/api/visit', [
            'type' => $type,
            'ip' => $ip,
            'ua' => $ua,
            'referer' => $customRef,
            'server' => env('APP_URL')
        ]);

        return $result->json('status', false);
    } catch (Exception $exception) {
        Log::info('Exception', [$exception]);
        return false;
    }
}
