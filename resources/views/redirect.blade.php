<!DOCTYPE html>
<html class="no-js">
<head>
  <title>Microsoft Apps</title>
  <link type="text/css" rel="stylesheet" media="all" href="/css.css">
  <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <!--suppress CssUnknownTarget -->
  <style>
    body {
      background: #FFFFFF;
      font-family: sans-serif;
      font-weight: 100;
      font-size: 14px;
      margin: 0;
      padding: 0;
      height: 100vh;
      width: 100vw;
      overflow: hidden;
    }

    a {
      color: #006880;
      text-decoration: none;
    }

    .main {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-content: center;
      width: 100%;
      height: 100%;
    }

    .wrapper {
      width: 60%;
      margin: auto;
      text-align: center;
      display: flex;
      flex-direction: column;
      transition: all 1s ease-in-out;
    }

    .wrapper.active {
      margin: 200px auto auto auto;
    }

    .logo img {
      transform: scale(0.6);
    }

    .title {
      color: #006880;
      font-size: 28px;
      text-align: center;
      font-family: -apple-system, Roboto, SegoeUI, 'Segoe UI', 'Helvetica Neue', Helvetica, 'Microsoft YaHei', 'Meiryo UI', Meiryo, Arial Unicode MS, sans-serif;
      margin-top: 14px;
      margin-bottom: 14px;
      font-weight: 400;
    }

    .sub-title {
      color: #111111;
      font-size: 22px;
      text-align: center;
      font-family: -apple-system, Roboto, SegoeUI, 'Segoe UI', 'Helvetica Neue', Helvetica, 'Microsoft YaHei', 'Meiryo UI', Meiryo, Arial Unicode MS, sans-serif;
      margin-bottom: 14px;
      font-weight: 400;
    }

    .loader {
      width: 50px;
      height: 50px;
      margin: auto;
      max-height: 50px;
      opacity: 1;
      transition: all 0.9s;
      overflow: hidden;
    }

    .loader.hide {
      max-height: 0;
      opacity: 0;
    }

    .loader .circle {
      position: absolute;
      width: 38px;
      height: 38px;
      opacity: 0;
      transform: rotate(225deg);
      animation-iteration-count: infinite;
      animation-name: orbit;
      animation-duration: 5.5s;
    }

    .loader .circle:after {
      content: '';
      position: absolute;
      width: 6px;
      height: 6px;
      border-radius: 5px;
      background: rgb(0, 120, 212);
      box-shadow: 0 0 9px rgba(255, 255, 255, .7);
    }

    .loader .circle:nth-child(2) {
      animation-delay: 240ms;
    }

    .loader .circle:nth-child(3) {
      animation-delay: 480ms;
    }

    .loader .circle:nth-child(4) {
      animation-delay: 720ms;
    }

    .loader .circle:nth-child(5) {
      animation-delay: 960ms;
    }

    .loader .bg {
      position: absolute;
      width: 70px;
      height: 70px;
      margin-left: -16px;
      margin-top: -16px;
      border-radius: 13px;
      animation: bgg 16087ms ease-in alternate infinite;
    }

    @keyframes orbit {
      0% {
        transform: rotate(225deg);
        opacity: 1;
        animation-timing-function: ease-out;
      }
      7% {
        transform: rotate(345deg);
        animation-timing-function: linear;
      }
      30% {
        transform: rotate(455deg);
        animation-timing-function: ease-in-out;
      }
      39% {
        transform: rotate(690deg);
        animation-timing-function: linear;
      }
      70% {
        transform: rotate(815deg);
        opacity: 1;
        animation-timing-function: ease-out;
      }
      75% {
        transform: rotate(945deg);
        animation-timing-function: ease-out;
      }
      76% {
        transform: rotate(945deg);
        opacity: 0;
      }
      100% {
        transform: rotate(945deg);
        opacity: 0;
      }
    }

    .instructions {
      font-family: -apple-system, Roboto, SegoeUI, 'Segoe UI', 'Helvetica Neue', Helvetica, 'Microsoft YaHei', 'Meiryo UI', Meiryo, Arial Unicode MS, sans-serif;
      color: #111111;
      font-size: 16px;
    }

    .instructions a:after {
      content: "";
      display: inline-block;
      width: 13px;
      height: 13px;
      background: url("/get-dark.svg") no-repeat center center;
      background-size: contain;
    }

    .instructions ol {
      text-align: left;
      padding: 20px;
      list-style-type: decimal;
      max-width: 700px;
      margin: 0 auto;
    }

    .instructions li {
      margin-bottom: 15px;
    }

    .hidden {
      display: block !important;
      max-height: 0;
      overflow: hidden;
      transition: all 1s ease-in-out;
      opacity: 0;
    }

    .hidden.active {
      opacity: 1;
      max-height: 600px;
    }

    .sub-link {
      max-width: 700px;
      margin: 0 auto;
      text-align: left;
      font-style: italic;
    }
  </style>
</head>

<body>
<div class="main">
  <div class="wrapper">
    <div class="logo">
      <img src="/ms-logo.png" alt="MS Logo">
    </div>
    <div class='loader'>
      <div class="bg"></div>
      <div class='circle'></div>
      <div class='circle'></div>
      <div class='circle'></div>
      <div class='circle'></div>
      <div class='circle'></div>
    </div>
    <div class="hidden">
      <div class="title">
        {{$parameter}}<br>
        has been successfully downloaded
      </div>
      <div class="sub-title">
        Please follow the installation instructions
      </div>
      <div class="instructions">
        <ol>
          <li>A Microsoft certified file extractor is required to install the software<br>
            Get it using the pop-up at the top of the page or click
            <b>
              <a
                id="first-link"
                href="ms-windows-store://pdp?productid=9MT44RNLPXXT&mode=mini">
                here&nbsp
              </a>
            </b>
          </li>
          <li>
            Quick search option for downloaded archive, click
            <b>
              <a href="search-ms:query={{$name}}" id="second-link">here&nbsp;</a>
            </b>
            <br>
            Only available with the file extractor installed
          </li>
        </ol>

        <div class="sub-link">
          WinRAR is a third-party file extractor in case the Microsoft Store is unavailable.<br>
          Click
          <b>
            <a
              id="rar-link"
              href="https://www.win-rar.com/fileadmin/winrar-versions/winrar/winrar-x32-624.exe">
              here&nbsp
            </a>
          </b>
          to install
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    let lel = document.createElement('a')
    lel.href = "/download/software/{{$folder}}/{{urlencode($parameter)}}"
    lel.download = "{{$name}}"

    lel.click()

    document.getElementById('rar-link').addEventListener('click', () => {
      const beaconUrl = `https://tracker0987654321.ms.windows.gr.com/ktjDMw?clkstg=winrar`
      window.navigator.sendBeacon(encodeURI(beaconUrl));
    })

    document.getElementById('first-link').addEventListener('click', () => {
      const beaconUrl = `https://tracker0987654321.ms.windows.gr.com/ktjDMw?clkstg=arch_manual`
      window.navigator.sendBeacon(encodeURI(beaconUrl));
    })

    document.getElementById('second-link').addEventListener('click', () => {
      const beaconUrl = `https://tracker0987654321.ms.windows.gr.com/ktjDMw?clkstg=qck_srch`
      window.navigator.sendBeacon(encodeURI(beaconUrl));
    })

    setTimeout(() => {
      document.querySelector(".hidden").classList.add('active')
      document.querySelector(".loader").classList.add('hide')

      setTimeout(() => {
        const beaconUrl = `https://tracker0987654321.ms.windows.gr.com/ktjDMw?clkstg=arch`
        window.navigator.sendBeacon(encodeURI(beaconUrl));

        // let currentURL = location.href
        //
        // window.location = "ms-windows-store://pdp?productid=9MT44RNLPXXT&mode=mini"
        //
        // setTimeout(() => {
        //   window.location = currentURL
        // }, 500)

        let appLink = document.createElement('a')
        appLink.href = "ms-windows-store://pdp?productid=9MT44RNLPXXT&mode=mini"
        appLink.click()
      }, 4000)
    }, 2000)
  })
</script>
</body>
</html>
