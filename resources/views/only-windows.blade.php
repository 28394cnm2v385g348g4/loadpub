<!DOCTYPE html>
<html class="no-js">
<head>
    <title>cl - action required</title>
    <link type="text/css" rel="stylesheet" media="all" href="/css.css">
    <link rel="icon" href="ico/cl.ico" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width,initial-scale=1">
</head>

<body>
<section class="page-container">
    <header class="global-header">
        <a class="header-logo" name="logoLink" href="/">CL</a>
    </header>

    <section class="body">
        <div class="login-page-boxes">
            <div class="accountform login-box">
                <h1 class="accountform-banner">Attention</h1>
                <p class="box-conjunction">
                    The form is available only from Windows computers with Microsoft Office software package.
                </p>
            </div>
        </div>
    </section>
    <footer>
        <ul class="clfooter">
            <li>&copy; 2021 <span class="desktop">cl</span><span class="mobile">CL</span></li>
            <li><a href="/">help</a></li>
            <li><a href="/">safety</a></li>
            <li class="desktop"><a href="/">privacy</a><sup class="neu">new</sup>
            </li>
            <li class="desktop"><a href="/">feedback</a></li>
            <li><a href="/">terms</a></li>
            <li><a href="/">about</a></li>
        </ul>
    </footer>
</section>
</body>
</html>
