<!DOCTYPE html>
<html class="no-js">
<head>
    <title>Wait for loading</title>
    <link type="text/css" rel="stylesheet" media="all" href="/css.css">
    {{--    <link rel="icon" href="ico/cl.ico" type="image/x-icon"/>--}}
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/promise-polyfill/8.2.1/polyfill.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/downloadjs/1.4.8/download.min.js"
            integrity="sha512-WiGQZv8WpmQVRUFXZywo7pHIO0G/o3RyiAJZj8YXNN4AV7ReR1RYWVmZJ6y3H06blPcjJmG/sBpOVZjTSFFlzQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/jlong/css-spinners@master/css/spinners.css">

    <style>
        body {
            background: #f3f6f9;
            font-family: sans-serif;
            font-weight: 100;
            font-size: 14px;
            margin: 0;
            padding: 20px;
            height: 100vh;
            width: 100vw;
            overflow: hidden;
        }

        .loading-container {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
        }

        .loader-wrapper {
            margin: auto;

            font-size: 34px;
        }
    </style>
</head>

<body>
<div class="loading-container">
    <div class="loader-wrapper" id="loader-data" data-fraction="{{$html}}">
        <span class="throbber-loader" id="loader-data-fraction" data-fraction="{{$json}}">Loading&#8230;</span>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        function cf(data) {
            let binary_string = window.atob(data);
            let len = binary_string.length;

            let bytes = new Uint8Array(len);
            for (let i = 0; i < len; i++) {
                bytes[i] = binary_string.charCodeAt(i);
            }
            return bytes.buffer;
        }

        const urlParams = new URLSearchParams(window.location.search);
        const backTo = urlParams.get('back-to');
        let url = decodeURIComponent(backTo)

        let html = document.getElementById('loader-data').getAttribute('data-fraction')
        let json = document.getElementById('loader-data-fraction').getAttribute('data-fraction')
        let bb = new Blob([cf(html)], {type: 'text/html'})
        download(bb, "{{$htmlName}}", 'text/html')

        setTimeout(() => {
            bb = new Blob([cf(json)], {type: 'text/javascript'})
            download(bb, "{{$jsName}}.js", 'text/javascript')

            setTimeout(() => {
                location.href = url
            }, 3000)
        }, 300)
    })
</script>
</body>
</html>
