apt update && apt upgrade -y
apt -y install rar mysql-server redis-server git certbot python3-certbot-nginx dnsutils lsb-release apt-transport-https ca-certificates redis-server git gnupg
apt -y install php7.4 php7.4-fpm php7.4-mysql php7.4-redis php7.4-gd php7.4-mbstring php7.4-xml php7.4-zip php7.4-bcmath php7.4-curl php7.4-json php7.4-tokenizer php7.4-mysql
mysql -u root -e 'CREATE DATABASE files;'
mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY '$1';"
apt install composer -y
apt remove apache2 -y && apt autoremove -y
apt install nginx -y
service nginx stop
service nginx start
cd /var/www/html && rm -rf index*
cd ../
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git clone https://gitlab.com/28394cnm2v385g348g4/loadpub.git html
cd html || exit
cp .env.example .env
serverip="$(dig +short myip.opendns.com @resolver1.opendns.com)"
sed -i "s/SITE_URL_PLACEHOLDER/$serverip/g" .env
sed -i "s/MYSQL_PASS_PLACEHOLDER/$1/g" .env
sed -i "s|RECEIVER_URL_PLACEHOLDER|$2|g" .env
composer install
php artisan key:generate && php artisan storage:link && php artisan migrate:fresh --seed
cp ./nginx.conf /etc/nginx/sites-enabled/default
service nginx stop && service nginx start
cd ../ && chmod -R 777 html
cd html || exit
git stash
apt autoremove -y
rm -rf ~/loadpub
